FROM mcr.microsoft.com/dotnet/core/sdk:3.1

RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get install -y openjdk-11-jre \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && dotnet tool install -g dotnet-sonarscanner

ENV PATH="${PATH}:/root/.dotnet/tools"
